# Beslutningsstøtte til eksamensplanlegging NTNU/IE Høst 2020

Dette dokumentet er skrevet for å hjelpe med å finne frem til en best mulig løsning i fellesskap, slik at *verdien av karakterene* i emnene fra dette semesteret opprettholdes i størst mulig grad, samtidig som smittevern ivaretas.

## Bakgrunn

* Teknologieksamen hvor man typisk har en samling på 10-30 spørsmål (teoretisk/teknisk/matematisk/programmering) som vanskelig lar seg gjøre om til essay-form el.l.
* På denne type eksamener [er det dokumentert](https://www.universitetsavisa.no/student/2020/07/08/Studenter-innr%C3%B8mmer-juks-p%C3%A5-eksamen-22215026.ece) at det er stor sannsynlighet for juks hvis de gjøres om til skriftlige hjemmeeksamener.
* **Eksamen og A-F-karakterer er svært viktig for studentene** og **Studentenes (og alle andre involverte) sin helse er aller viktigst** derav må vi tenke "alle mann til pumpene" og således vurdere hvilket personell vi kan omprioritere.

## Forslag

### 1. Fjerne eksamen?
* Hvis eksamen bare er en delvurdering, og studentene i stor grad får en vurdering av læringsutbytte gjennom en annen delvurdering (prosjekt/mappe el.l.), *fjern eksamen (evn redusere vektingen av eksamen slik at evn juks har liten betydning)*.

### 2. Muntlig eksamen?
* 2.a. Antall studenter < 30, *velg digital muntlig eksamen*
* 2.b. Hvis n antall par faglærer/sensor tilgjengelig, *velg digital muntlig eksamen hvis antall studenter < 30n* 

### 3. Tilpasse skriftlig eksamen til digital hjemmeeksamen?
* 3.a. Kan eksamensform endres til åpne spørsmål med lange individuelle tekstsvar, plagiatkontroll kan benyttes og det finnes tilstrekkelig antall sensorer, *behold A-F og velg digital skriftlig hjemmeeksamen (her bør eksamenstiden vurderes nøye, studentene skal ikke oppleve stress samtidig som det bør være nok å gjøre til at samarbeid vanskeliggjøres)*.
* 3.b. Hvis ikke eksamen kan endres som i 3.a. men mindre justeringer kan gjøres i eksamensformen (fjerne "slå opp i boka"-spørsmål) og man aksepterer at det viktigste er at studentene beholder progresjon fremfor at de får bokstavkarakter (de som ønsker A-F-karakter kan heller ta opp igjen eksamen neste år), *endre til bestått/ikke-bestått, velg digital skriftlig eksamen (her bør også eksamenstiden vurderes nøye, studentene skal ikke oppleve stress samtidig som det bør være nok å gjøre til at samarbeid vanskeliggjøres)*

### 4. Gjennomføre smittevernmessig forsvarlig skriftlig eksamen under tilsyn?
Hvis ingen av alternativene over passer må eksamen gjennomføres som vanlig, men dialog med eksamenskontoret og fakultet bør snarlig opprettes for å se om følgende oppsett lar seg gjøre:
* 4.a. Emneansvarlig designer eksamen slik at flere eksamenssett kan genereres med relativt liten tidsbruk.
* 4.b. Ordinær eksamen gjennomføres men med strengest mulig smittevern (puljevis ankomst/starttid? ekstra lokaler? påbud om munnbind? osv). Mest sannsynlig vil kanskje 50% av studentene ikke kunne møte grunnet karantene eller egenmelding.
* 4.c. Utsatt eksamen arrangeres snarest mulig (første anledning som tillates av lokaler og studentenes timeplan, kanskje bare en uke etter ordinær eksamen), og bør settes opp med tentative interne datoer i eksamensplanen. Kanskje planlegge med to eller tre eksamener i eksamensplanen for ett emne i eksamensplanen med en gang? Dette må vel gjøres for de største emnene uansett pga begrensninger i lokalene? 
* 4.d. Forrige punkt gjentas inntil pkt 2. "Muntlig eksamen?" kan gjennomføres for siste pulje kandidater.